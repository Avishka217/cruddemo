const express = require("express");
const bodyParser = require("body-parser");
const cors = require("cors");
const mysql = require("mysql");

const app = express();
const port = 5000;

// MySQL database connection
const connection = mysql.createConnection({
  host: "localhost",
  user: "root",
  password: "root",
  database: "mern_crud",
});

connection.connect((err) => {
  if (err) {
    console.error("Error connecting to MySQL database: ", err);
    return;
  }
  console.log("Connected to MySQL database");
});

app.use(bodyParser.json());
app.use(cors());

// Create a task
app.post("/api/tasks", (req, res) => {
  const { name, completed } = req.body;
  const sql = "INSERT INTO tasks (name, completed) VALUES (?, ?)";
  connection.query(sql, [name, completed], (err, result) => {
    if (err) {
      console.error("Error inserting task: ", err);
      res.status(500).send("Error inserting task");
      return;
    }
    res.status(201).send("Task added successfully");
  });
});



// Read all tasks
app.get("/api/tasks", (req, res) => {
  const sql = "SELECT * FROM tasks";
  connection.query(sql, (err, results) => {
    if (err) {
      console.error("Error fetching tasks: ", err);
      res.status(500).send("Error fetching tasks");
      return;
    }
    res.json(results);
  });
});

// Read a single task by ID
app.get('/api/tasks/:id', (req, res) => {
  const { id } = req.params;
  const sql = 'SELECT * FROM tasks WHERE id = ?';
  connection.query(sql, [id], (err, result) => {
    if (err) {
      console.error('Error fetching task: ', err);
      res.status(500).send('Error fetching task');
      return;
    }
    if (result.length > 0) {
      res.json(result[0]); // Send the task as JSON response
    } else {
      res.status(404).send('Task not found'); // Task with given ID not found
    }
  });
});

// Update a task
app.put("/api/tasks/:id", (req, res) => {
  const { id } = req.params;
  const { name, completed } = req.body;
  const sql = "UPDATE tasks SET name=?, completed=? WHERE id=?";
  connection.query(sql, [name, completed, id], (err, result) => {
    if (err) {
      console.error("Error updating task: ", err);
      res.status(500).send("Error updating task");
      return;
    }
    res.send("Task updated successfully");
  });
});

// Delete a task
app.delete("/api/tasks/:id", (req, res) => {
  const { id } = req.params;
  const sql = "DELETE FROM tasks WHERE id=?";
  connection.query(sql, [id], (err, result) => {
    if (err) {
      console.error("Error deleting task: ", err);
      res.status(500).send("Error deleting task");
      return;
    }
    res.send("Task deleted successfully");
  });
});

app.listen(port, () => {
  console.log(`Server is running on port ${port}`);
});
